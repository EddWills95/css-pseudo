# Acme Company CSS Pseudo Practice #

Using CSS to add FontAwesome icons and react to changes in state in a HTML document.

(FontAwesome icons are listed at http://fontawesome.io/icons/, click on icon to get unicode and <i> tag example.)

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open your local copy folder with Sublime, there is a HTML document and CSS style sheets.
2. Add your style rules to style.css, this file is linked to from acme-company.html.
3. Open acme-company.html by typing in terminal 'open acme-company.html', this is a very basic company webpage with dummy text.
4. Add FontAwesome icons to nav items and widget titles, examples are in acme-company.html and style.css.
5. Add style rules that change the styling of nav items on change of state, examples are in acme-company.html and style.css.
6. You can see your progress by typing in terminal 'open acme-company.html'.

(The declarations you wrote in style.css took precedence over the ones found in starter.css because of the order they are loaded in.)
